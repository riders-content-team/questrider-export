#!/usr/bin/env python

import os
import json
import subprocess

import requests


class ResultsHandler:
    """See Paintball August 2021 challenge project for full implementation. Abbreviated here for simplicity."""

    def __init__(self):
        self.challenge_id = os.environ.get('RIDERS_CHALLENGE_ID', None)
        self.project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        self.branch_id = os.environ.get('RIDERS_PROJECT_BRANCH_ID', None)
        self.token = os.environ.get('RIDERS_AUTH_TOKEN', None)
        self.host = os.environ.get('RIDERS_HOST', None)

        self.user_name, self.remote_name, self.branch_name, self.commit_id = "", "", "", ""

        self.git_dir = '/workspace/src/.git'
        if not os.path.isdir(self.git_dir):
            return

        # get remote name
        popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE, text=True)
        remote_url, error = popen.communicate()
        if not error:
            remote_url = remote_url.rstrip('\n').rsplit('@')
            self.user_name = remote_url[0].rsplit(':')[1][2:].rstrip('\n')
            self.remote_name = remote_url[1].rstrip('\n')

        # get branch
        popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE, text=True)
        branch_name, error = popen.communicate()
        if not error:
            self.branch_name = branch_name.rstrip('\n')

        # get commit id
        popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE, text=True)
        commit_id, error = popen.communicate()
        if not error:
            self.commit_id = commit_id.rstrip('\n')

    @property
    def props(self):
        return {
            'user_name': self.user_name,
            'branch_name': self.branch_name,
            'remote_name': self.remote_name,
            'commit_id': self.commit_id,
        }