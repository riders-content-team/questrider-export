import os

from fastapi import FastAPI, Request
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from submission import ResultsHandler


# Starts Api Server
app = FastAPI()
game_results_handler = ResultsHandler()

challenge_id = os.environ.get('RIDERS_CHALLENGE_ID', None)
project_id = os.environ.get('RIDERS_PROJECT_ID', None)
branch_id = os.environ.get('RIDERS_PROJECT_BRANCH_ID', None)
token = os.environ.get('RIDERS_AUTH_TOKEN', None)
host = os.environ.get('RIDERS_HOST', None)
allowed_origin = os.getenv("GODOT_PYTHON_URI") if os.getenv("GODOT_PYTHON_URI") else "127.0.0.1:8000"
user_code_uri = os.getenv("USER_CODE_URI") if os.getenv("USER_CODE_URI") else "../user-code"

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    response = await call_next(request)
    response.headers["Cross-Origin-Opener-Policy"] = "same-origin"
    response.headers["Cross-Origin-Embedder-Policy"] = "require-corp"
    response.headers["Cross-Origin-Resource-Policy"] = "cross-origin"
    return response


@app.get("/user-code/{file_name}")
async def user_code(file_name):
    """
    Serves the user code to the client.
    """
    with open(f"{user_code_uri}/{file_name}", "r") as f:
        file = f.read()
        return file

# Serves static files contained in export folder to client.

@app.get("/mutex.js")
async def mutex_js():
    mutex_path = "../godot-web-export/mutex/mutex.js"
    response = FileResponse(mutex_path)
    response.headers["content-type"] = "application/javascript"
    return response

@app.get("/webworker.js")
async def mutex_js():
    mutex_path = "../godot-web-export/webworker/webworker.js"
    response = FileResponse(mutex_path)
    response.headers["content-type"] = "application/javascript"
    return response

@app.get("/get_user_credentials")
async def get_user_credentials():
    props = game_results_handler.props
    resp = {
        "host":host,
        "project_id":project_id,
        "challenge_id":challenge_id,
        "branch_id":branch_id,
        "token":token,
        "user_name": props["user_name"],
        "branch_name": props["branch_name"],
        "remote_name": props["remote_name"],
        "commit_id": props["commit_id"]
    }
    return resp

# app.mount("/user-code", StaticFiles(directory= user_code_uri), name="user-code-file-server")
if "RIDERS_HOST" in os.environ:
    app.mount("/project", StaticFiles(directory= "/workspace/src/_lib/"), name="project-materials-server")
app.mount("/", StaticFiles(directory= "../godot-web-export"), name="godot-web-file-server")
