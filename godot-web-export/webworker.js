importScripts("https://cdn.jsdelivr.net/pyodide/v0.21.0/full/pyodide.js");
importScripts("./mutex.js");

function sleep(seconds) {
    pyodide.checkInterrupt();
    const milliseconds = seconds * 1000;
    Atomics.wait(self.sleep_sab, 0, 0, Math.max(1, milliseconds|0));
}
var robot = {name: "DirtRider"};

var _robot_function_names = ["move","rotate"];

function _initRobot() {
    robot["is_ok"] = (args) => {
        _sendBlockingCommand({name: "call_method", method_name: "process_frame", "args": args, "lock_name": "process_frame"});
        return true;
    };
    robot["move_forward"] = (args) => {_sendBlockingCommand({name: "call_method", method_name: "move_forward", "args": args, "lock_name": "move_forward"});};
    robot["move_backward"] = (args) => {_sendBlockingCommand({name: "call_method", method_name: "move_backward", "args": args, "lock_name": "move_backward"});};
    robot["turn_left"] = (args) => {_sendBlockingCommand({name: "call_method", method_name: "turn_left", "args": args, "lock_name": "turn_left"});};
    robot["turn_right"] = (args) => {_sendBlockingCommand({name: "call_method", method_name: "turn_right", "args": args, "lock_name": "turn_right"});};
}

function _sendCommand(command) {
    pyodide.checkInterrupt();
    self.postMessage({
        command: command
    });
}

function _sendBlockingCommand(command) {
    _sendCommand(command);
    self.func_mutex.force_lock();
}
async function _onUserCodeOutput(msg) {
    self.postMessage({user_code_output: msg});
}
 
async function loadPyodideAndSabs(context) {
    pyodide = await loadPyodide({stdout:this._onUserCodeOutput});
    self["func_mutex"] = new Mutex(context.func_mutex_sab);
    self["sleep_sab"] = context.sleep_sab;

    pyodide.setInterruptBuffer(context.interrupt_buffer_sab);
    self.pyodide_interrupt_buffer = context.interrupt_buffer_sab
    _sendCommand({name: "init_done"});
}

async function runUserCode(context) {
    // Ensure self.pyodide_interrupt_buffer is set to 0 at the beginning
    Atomics.exchange(self.pyodide_interrupt_buffer, 0, 0);

    self.robot_msg = context.robot_msg;
    _initRobot();
    try {
        pyodide.runPython(context.user_code);
    } catch (error) {
        self.postMessage({
            user_code_error: error.message
        });
    }
    _sendCommand({name: "init_done"});
}

self.onmessage = async (event) => {
    const { ...context } = event.data;

    if (context.command == "init") {
        await loadPyodideAndSabs(context);
    } else if (context.command == "run_user_code") {
        runUserCode(context);
    }
};
