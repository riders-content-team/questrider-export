// TODO: This is a temporary solution for a proof-of-concept demo.
// This file will be generated from the typescript with webpack in the future.
const locked = 1;
const unlocked = 0;

class Mutex {
   /**
     * Instantiate Mutex.
     * The mutex will use sab as a backing array.
     * @param {SharedArrayBuffer} sab SharedArrayBuffer.
     */
    constructor(sab) {
        this._sab = sab;
        this._mu = new Int32Array(this._sab);
    }

    /**
     * Puts the caller thread to sleep if mutex is already locked.
     * If mutex is unlocked, locks the muxed and caller thread keeps working.
     */
    lock() {
        for(;;) {
            if (Atomics.compareExchange(this._mu, 0, unlocked, locked) == unlocked) {
                return;
            }
            Atomics.wait(this._mu, 0, locked);
        }
    }

    force_lock() {
        if (Atomics.compareExchange(this._mu, 0, unlocked, locked) != unlocked) {
            throw new Error("Mutex is in inconsistent state: force_lock on locked Mutex.");
        }
        Atomics.wait(this._mu, 0, locked);
    }

    unlock() {
        if (Atomics.compareExchange(this._mu, 0, locked, unlocked) != locked) {
            throw new Error("WorkerMutex is in inconsistent state: unlock on unlocked Mutex.");
        }
        Atomics.notify(this._mu, 0, 1);
    }
}
