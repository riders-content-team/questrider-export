import math

class Vector3D:
    
    def __init__(self, x,y,z) -> None:
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)

    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)

    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)

    def __truediv__(self, scalar):
        return Vector3D(self.x / scalar, self.y / scalar, self.z / scalar)

    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z

    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

    def length_squared(self):
        return (self.x * self.x + self.y * self.y + self.z * self.z)